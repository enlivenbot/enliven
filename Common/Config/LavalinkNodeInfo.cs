namespace Common.Config {
    public struct LavalinkNodeInfo {
        public string RestUri { get; set; }
        public string WebSocketUri { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}